﻿#include <iostream>
class Animal
{
public:
    virtual void Sdelat_zvuk() const = 0;
};

class Koshka : public Animal
{
public:
    void Sdelat_zvuk() const override
    {
        std::cout << "Myau\n";
    }
};

class Sobaka : public Animal
{
public:
    void Sdelat_zvuk() const override
    {
        std::cout << "Gaf-gaf\n";
    }
};

class Korova : public Animal
{
public:
    void Sdelat_zvuk() const override
    {
        std::cout << "Muuuu\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Koshka();
    animals[1] = new Sobaka();
    animals[2] = new Korova();

    for (Animal* a : animals)
        a->Sdelat_zvuk();
    return 0;
}